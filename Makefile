.PHONY: all
gsl_flags := $(shell pkg-config --libs --cflags gsl) -DHAVE_INLINE
commonflags := -Wall -Wextra -std=c99 -pedantic -Wno-unknown-pragmas -Wshadow -Wpointer-arith
commonflags += $(CFLAGS)

all: eemd1

eemd1: src/eemd1.c
	gcc $(commonflags) -ggdb -I../src $^ -L.. -Wl,-rpath,.. $(gsl_flags) -leemd -o $@
