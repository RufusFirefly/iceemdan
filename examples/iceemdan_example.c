/* Copyright 2018 Charles David Coleman

 * This file is part of libeemd.

 * libeemd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * libeemd is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with libeemd.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_trig.h>
const double pi = M_PI;

#include "eemd.h"

const size_t ensemble_size = 50;
const unsigned int S_number = 4;
const unsigned int num_siftings = 50;
const double noise_strength = 0.02;
const unsigned long int rng_seed = 0;
const char outfile[] = "iceemdan_example.out";

const size_t N = 999;

int main(void) {
	libeemd_error_code err;
	// As an example decompose sine mixture as in the original ICEEMDAN paper, equations 9 and 10
	double* inp = malloc(N*sizeof(double));
	memset(inp, 0x00, N*sizeof(double));
	for (size_t i=0; i<1000; i++) {
		inp[i] = gsl_sf_sin(2*M_PI * 0.65 * i);
		if ((i >= 500) && (i < 750)) {inp[i] += gsl_sf_sin(2 * M_PI * .255 * (i - 500));}
	}
	// Write input to file
	FILE* fp = fopen(outfile, "w");
	for (size_t j=0; j<N; j++) {
		fprintf(fp, "%f ", inp[j]);
	}
	fprintf(fp, "\n");
	// Allocate memory for output data
	size_t M = emd_num_imfs(N);
	double* outp = malloc(M*N*sizeof(double));
	// Run ICEEMDAN
	err = iceemdan(inp, N, outp, M, ensemble_size, noise_strength, S_number, num_siftings, rng_seed);
	if (err != EMD_SUCCESS) {
		emd_report_if_error(err);
		exit(1);
	}
	// Write output to file
	for (size_t i=0; i<M; i++) {
		for (size_t j=0; j<N; j++) {
			fprintf(fp, "%f ", outp[i*N+j]);
		}
		fprintf(fp, "\n");
	}
	printf("Done!\n");
	// Cleanup
	fclose(fp);
	free(inp); inp = NULL;
	free(outp); outp = NULL;
}
